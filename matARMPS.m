%function matARMPS()
%% Creating output file
%delete('ARMPS_INPUT.dat')
%diary('ARMPS_INPUT.dat')
%diary on

density = 162;
coalStrength = 900;
%% Import data from ARMPS DB
[~, ~, raw] = xlsread('SMALL_ARMPS_DB.xls','ARMPS 2010 Database');
raw = raw(2:end,:);

% Replace non-numeric cells with 0.0
R = cellfun(@(x) ~isnumeric(x) || isnan(x),raw); % Find non-numeric cells
raw(R) = {0.0}; % Replace non-numeric cells

% Create output variable
inputfile = cell2mat(raw);

% Clear temporary variables
clearvars raw R;
clc
%% Depth Condition
% for all depths depthCondition = 0
% for deep cover depthCondition = 1
% for shallow cover depthCondition = 2
totalCase       = size(inputfile,1);

depthCondition  = 0;

delRowCounter   = 0;
if depthCondition == 1
    for i=1:totalCase
        if inputfile(i,4)<750
            delRowCounter       = delRowCounter+1;
            rowNumberToDelete(delRowCounter)  = i;
        end
    end
else if depthCondition == 2
       for   i=1:totalCase
            if inputfile(i,4)>=750
                delRowCounter       = delRowCounter+1;
                rowNumberToDelete(delRowCounter)  = i;
            end
       end
    end
end
if delRowCounter ~= 0
inputfile(rowNumberToDelete,:)=[];
end
clearvars totalCase i rowNumberToDelete depthCondition delRowCounter;
   
   
   %% Define imported parameters
caseNumber      = inputfile(:,37);
entryHight      = inputfile(:,3);
depthOfCover    = inputfile(:,4);
entryWidth      = inputfile(:,5);
crosscutSpacing = inputfile(:,6);
crosscutAngle   = inputfile(:,7);
numberOfEntry   = inputfile(:,8);
entrySpacing    = inputfile(:,9);
loadingCondition= inputfile(:,18);
frontGobExtent  = inputfile(:,19);
firstGobExtent  = inputfile(:,20);
secondGobExtent = inputfile(:,21);
firstBarPillar  = inputfile(:,22);
secondBarPillar = inputfile(:,23);
firstSlabCut    = inputfile(:,24);
secondSlabCut   = inputfile(:,25);
CMRR            = inputfile(:,26);
RSR             = inputfile(:,27);
caprockThickness= inputfile(:,28);
seamStrngth     = inputfile(:,33);
HGI             = inputfile(:,34);
caseSuccess     = inputfile(:,35);

numberofCases   = size(inputfile,1);

clearvars inputfile;




%% AMZ Calculation
amzWidth    = (numberOfEntry-1) .* entrySpacing;    %Must change for LC 2
amzBreath   = 5*(sqrt(depthOfCover));               %checked with ARMPS
amzArea     = amzWidth .* amzBreath;                %checked with ARMPS

%checked with ARMPS

%% Pillar Calculations
%pWidth  length of pillar between crosscuts 
%pLength length of pillars between entries
%pArea
%xcutAngle crosscut angle in radiance
xcutAngle   = crosscutAngle * pi /180;
pWidth      = (crosscutSpacing -(entryWidth./sin(xcutAngle))).*sin(xcutAngle);
pLength     = (entrySpacing - entryWidth).*(1./sin(xcutAngle));
pArea       = pWidth.*pLength; %checked with ARMPS


pAreaRow  = pArea .* (numberOfEntry -1);
pAreaTotal = pAreaRow .* (amzBreath./crosscutSpacing); %checked with ARMPS


%% Recovery Rate Calculation
panelWidth   = amzWidth;                %checked with ARMPS           
recovery     = (crosscutSpacing.*entrySpacing - pArea)...
    ./(crosscutSpacing.*entrySpacing);  %checked with ARMPS

%% Mark-Bieniawski Pillar Strength



%% Pressure Arch Factor

archFactor  = 1 - (0.28*log(depthOfCover./amzWidth)); %checked with ARMPS
for i=1:size(archFactor,1)
    if archFactor(i) > 1
        archFactor(i) = 1;
    end
end

%% Development Load 
%checked with ARMPS calculation slide


%Development Load in AMZ area
dLoad       = amzArea .* depthOfCover * density .* archFactor; 
%% Pillar Strength Calculation

pMaxLength = max (pLength,pWidth);
pMinLength = min (pLength,pWidth);

pStrength = coalStrength *(0.64+(0.54*(pMinLength./entryHight)- ...
    0.18*(pMinLength.*pMinLength./(entryHight.*pMaxLength)))); %checked with ARMPS
% Single Pillar Load Bearing Capasity (lbs)
pLoadBearing = pStrength .* pArea * 144; %checked with ARMPS (ARMPS use tons)
% All Pillars in AMZ area Bearing Capasity
pLoadBearingTotal = pAreaTotal .* pStrength .*144; 

%% Development SF
SF = pLoadBearingTotal ./ dLoad; %checked with ARMPS (ARMPS use tons)

%% R-Factor
%abutmentInfluenceZone = 9.3 * (entryHight).^0.5;
%R = 1 -((( abutmentInfluenceZone - depthOfCover)./depthOfCover).^3);
